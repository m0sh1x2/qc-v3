module gitlab.com/m0sh1x2/qc-v3

go 1.19

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	golang.org/x/crypto v0.9.0
)

require (
	github.com/felixge/httpsnoop v1.0.1 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
