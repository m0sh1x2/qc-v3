package prompts

import (
	"database/sql"
	"log"
)

type Prompt struct {
	ID      int
	Name    string
	Content string
}

func Create(db *sql.DB, prompt *Prompt) error {
	_, err := db.Exec("INSERT INTO prompts (name, content) VALUES ($1, $2)", prompt.Name, prompt.Content)
	if err != nil {
		log.Printf("failed to create prompt: %v", err)
		return err
	}
	return nil
}

func Read(db *sql.DB, id int) (*Prompt, error) {
	row := db.QueryRow("SELECT id, name, content FROM prompts WHERE id = $1", id)
	prompt := &Prompt{}
	err := row.Scan(&prompt.ID, &prompt.Name, &prompt.Content)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil // Prompt not found
		}
		log.Printf("failed to read prompt: %v", err)
		return nil, err
	}
	return prompt, nil
}

func ReadAll(db *sql.DB) ([]Prompt, error) {
	rows, err := db.Query("SELECT id, name, content FROM prompts")
	if err != nil {
		log.Printf("failed to get prompts: %v", err)
		return nil, err
	}
	defer rows.Close()

	var prompts []Prompt
	for rows.Next() {
		prompt := Prompt{}
		if err := rows.Scan(&prompt.ID, &prompt.Name, &prompt.Content); err != nil {
			log.Printf("failed to scan prompt: %v", err)
			return nil, err
		}
		prompts = append(prompts, prompt)
	}

	if err := rows.Err(); err != nil {
		log.Printf("failed to read prompts: %v", err)
		return nil, err
	}

	return prompts, nil
}

func Update(db *sql.DB, prompt *Prompt) error {
	_, err := db.Exec("UPDATE prompts SET name = $1, content = $2 WHERE id = $3", prompt.Name, prompt.Content, prompt.ID)
	if err != nil {
		log.Printf("failed to update prompt: %v", err)
		return err
	}
	return nil
}

func Delete(db *sql.DB, id int) error {
	_, err := db.Exec("DELETE FROM prompts WHERE id = $1", id)
	if err != nil {
		log.Printf("failed to delete prompt: %v", err)
		return err
	}
	return nil
}
