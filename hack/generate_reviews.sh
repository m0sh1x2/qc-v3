API_URL="http://localhost:8080"


# Loop to generate and save 100 random reviews
for ((i=1; i<=100; i++)); do
  # Generate random review data
  topic="Review $i"
  rating_tags='["tag1", "tag2", "tag3", "tag4", "tag5"]'
  short_summary="Summary $i"
  support_agent_quality=$((RANDOM%5+1))
  quality_score_reason="Reason $i"

  # Construct the review JSON
  review="{\"topic\":\"$topic\",\"rating_tags\":$rating_tags,\"short_summary\":\"$short_summary\",\"support_agent_quality\":$support_agent_quality,\"quality_score_reason\":\"$quality_score_reason\"}"

  # Make a curl POST request to save the review
  curl -X POST -H "Content-Type: application/json" -d "$review" "$API_URL"

  # Add a sleep delay between requests (optional)
#   sleep 1
done
