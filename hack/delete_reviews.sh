#!/bin/bash

# Define the API endpoint URL
API_URL="http://localhost:8080"

# Loop to make DELETE requests for each review ID
for ((id=1; id<=100; id++)); do
  # Make a curl DELETE request to remove the review by ID
  curl -X DELETE "$API_URL/?id=$id"

  # Add a sleep delay between requests (optional)
  sleep 1
done