import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewFormComponent } from './review-form/review-form.component';
import { ReviewListComponent } from './review-list/review-list.component';
import { TaskListComponent } from './task-list/task-list.component';
import { PromptsComponent } from './prompts/prompts.component';
import { ChatHistoryComponent } from './chat-history/chat-history.component';


const routes: Routes = [
  { path: 'review-form', component: ReviewFormComponent },
  { path: 'task-list', component: TaskListComponent },

  { path: 'reviews', component: ReviewListComponent },
  { path: 'prompts', component: PromptsComponent },
  { path: 'chats', component: ChatHistoryComponent }


];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
