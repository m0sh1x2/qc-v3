import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface Review {
  topic: string;
  rating_tags: string[];
  short_summary: string;
  support_agent_quality_score: number;
  quality_score_reason: string;
}

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.scss']
})
export class ReviewFormComponent implements OnInit {
  reviewForm: FormGroup;
  response: string | null = null;
  characterCount: number = 0;
  exampleMessage: string = "Our hosting is optimized for heavy and highly available, cross-region WordPress and WooCommerce sites that run on the edge with object cache enabled"
  buttonContents: any[] = [{ visible: 'Exp 4', hidden: 'Button 4 Content' }];



  updateCharacterCount(): void {
    const prompt = this.reviewForm.get('prompt')?.value;
    this.characterCount = prompt?.length || 0;
  }
  // buttonContents: { visible: string, hidden: string }[] = [
  updateHiddenValue(): void {

    this.buttonContents = [

      {
        visible: 'QC', hidden: `Below is an instruction that describes a task. Write a response that appropriately completes the request.

### Instruction:

You are given input text which is a text from a support agent that must be verified if true. The information may be partial but it always must be formatted in the requested output.

The provided message must be categorized and then result must be one of the following topics which are named <<target_list>>:
- Security
- SSL Certificates
- WordPress Hosting
- DevOps
- Kubernetes
- Datacenter Question
- Support
- Unknown

The output must always be only one of the items in the list and nothing else.

The support_agent_quality_score is rated from 1 to 10 and should be focused on the categorized data from the <<target_list>>
The quality_score_reason provides a short description and reason of the value of support_agent_quality_score and is evaluated based on the <<targed_list>> topics
The rating_tags is an array of tags that are summarized from the input text.

Format the output as valid json that has the topic, rating_tags, a short_summary and support_agent_quality_score for the answers, quality_score_reason
Provide only the json output and nothing else. All of the provided information is experimental and for educational purposes. The output is always only json.

In case there is another type of intent provide json data as well.
--------------------
This is the support agent message that you must analyze: 
${this.exampleMessage}

### Response` },
      {
        visible: 'Alerts', hidden: `Below is an instruction that describes a task. Write a response that appropriately completes the request.

### Instruction

You are given several Nagios Alerting alarms based on severity levels from Info, Warning and Critical.

Based on the alarm you must provide instructions on running commands that will resolve the alarms.

If you can't find a solution you should return a response like: Thought: I might not be able to resolve this issue.
If you can resolve the issue you must return a response: Thought: I can resolve the issue with the following bash commands <<bash code here>>
If the alarm is with Critical state you must decide if it is very bad and you will need to call a human. Example response for that: Thought: I should call a human on the phone.

Provide several short 10 word responses for every alarm that is listed below:
-----------
Critical: Datacenter under water
Warning: Someone slipped on a cable in the datacenter
Warning: Root file system is low on disk space.
Info: Medium amount of http request on site obunga.wpx

### Response` }, {
        visible: '🤣 Alerts', hidden: `Below is an instruction that describes a task. Write a response that appropriately completes the request.

### Instruction

You are given several Nagios Alerting alarms based on severity levels from Info, Warning and Critical.

Based on the alarm you must provide instructions on running commands that will resolve the alarms.

If you can't find a solution you should return a response like: <<emoji>> Thought: I might not be able to resolve this issue.
If you can resolve the issue you must return a response: <<emoji>> Thought: I can resolve the issue with the following bash commands <<bash code here>>
If the alarm is with Critical state you must decide if it is very bad and you will need to call a human. Example response for that: <<emoji>> Thought: I should call a human on the phone. 

Provide several short funny and illogical 10 word responses and an ascii emoji for every alarm that is listed below:
-----------
Critical: Datacenter under water
Warning: Someone slipped on a cable in the datacenter
Warning: Root file system is low on disk space.
Info: Medium amount of http request on site obunga.wpx

### Response` },
      {
        visible: 'Firewall', hidden: `Below is an instruction that describes a task. Write a response that appropriately completes the request.

### Instruction

You are a WordPress mod_security firewall generator that reads a Proof Of Concept vulnerabilities and then outputs a mod_security that will defend and protect the site with a rule rule with description on how to fix it.

You also provide instructions on how to test multiple functionalities of the WordPress site and if they will work correctly.

The tests must be at least 3 example curl commands to the WordPress site. 

The first mod_security firewall rule that you must write is about this exploit:

-----------

http://<WORDPRESS_SITE>/wp-admin/edit.php?post_type=acf-field-group&post_status=xxxxxxx” onload=alert(document.domain) xxx=”


### Response

In order to patch this vulnerability
` },
      {
        visible: 'k8s', hidden: `Below is an instruction that describes a task. Write a response that appropriately completes the request.

### Instruction

You are given the task to create an initial documentation in Markdown regarding a kubernetes cluster running a highly available k3s setup on a cross-region setup.

The first parts of the documentation must describe the infrastructure, hosts, hardware, cpu, memory, motherboard, networking and other data-center related items/solutions.

The second part should be a short overview on all of the components that are running in the kubernetes cluster including an example micro-service architecture.

You should also include what is DevOps and how to implement a ML setup with Large Language Models on the whole infrastructure, teams, automation and other aspects.

-----------

### Response

Here is the documentation and plan in markdown:` },
      {
        visible: 'Template', hidden: `Below is an instruction that describes a task. Write a response that appropriately completes the request.

### Instruction

-----------

### Response

` }
    ];
  }

  isLoading: boolean = false;

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.reviewForm = this.formBuilder.group({
      prompt: ['']
    });
  }


  ngOnInit(): void {
    this.updateHiddenValue()
  }
  formatInvalidJSON(jsonString: string): string {
    // Replace ":" with ": "
    jsonString = jsonString.replace(/^\s*:\s*/, '');

    // Replace missing commas ","
    jsonString = jsonString.replace(/(\]|}),(\s*)([a-zA-Z0-9"])/g, '$1,$2$3');

    return jsonString;
  }
  onSubmit(): void {
    this.isLoading = true; // Set isLoading to true when the generate button is clicked

    const prompt = this.reviewForm.get('prompt')?.value;
    if (prompt) {
      const url = `http://localhost:8080/prompt?prompt=${encodeURIComponent(prompt)}`;

      this.http.get(url).subscribe(
        (data: any) => {
          this.response = this.formatInvalidJSON(data.result);
          this.isLoading = false; // Set isLoading to false when the response is received

          if (this.response) {
            try {
              const jsonResponse = JSON.parse(this.response);
              const review = {
                topic: jsonResponse.topic,
                rating_tags: jsonResponse.rating_tags,
                short_summary: jsonResponse.short_summary,
                support_agent_quality_score: jsonResponse.support_agent_quality_score,
                quality_score_reason: jsonResponse.quality_score_reason
              };

              // this.saveReview(review);
            } catch (error) {
              console.log('Error parsing JSON response:', error);
            }
          }
        },
        (error) => {
          console.log('Error:', error);
          this.isLoading = false; // Set isLoading to false on error as well
        }
      );
    }
  }

  generateContent(): void {
    const content = 'Button 1 Content'; // Replace with your desired content
    this.reviewForm.get('prompt')?.setValue(content);
    this.updateCharacterCount();
  }

  expandContent(content: string): void {
    this.reviewForm.get('prompt')?.setValue(content);
    this.updateCharacterCount();
  }

  saveReview(review: any): void {
    const url = 'http://localhost:8080/reviews';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    // Dirty fix for the review

    this.http.post(url, review, httpOptions).subscribe(
      (data: any) => {
        console.log('Review saved:', data);
      },
      (error) => {
        console.log('Error saving review:', error);

        // Filter for specific error message
        if (error.error && typeof error.error === 'string' && error.error.includes("invalid character ':'")) {
          console.log('Invalid JSON format. Please check the review data.');
          // Handle the error specific to the invalid JSON format
          // Additional error handling logic can be added here
          return;
        }

        // Handle other errors
        // Additional error handling logic can be added here
      }
    );
  }


}

