import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { finalize, switchMap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-chat-history',
  templateUrl: './chat-history.component.html',
  styleUrls: ['./chat-history.component.scss']
})
export class ChatHistoryComponent implements OnInit {
  chats: any[] = [];
  displayedColumns: string[] = ['id', 'startDate', 'endDate', 'rating', 'actions'];
  chatMessages: any[] = []; // Declare the chatMessages property
  chatId: string = "8";
  selectedDifficulty: string = 'easy';

  constructor(private http: HttpClient) { }

  ngOnInit() {
    console.log('chat history');

    // Test for Kong API + Cors
    // const url = 'http://192.168.88.120:8000/api'; // Replace with your actual Kong API endpoint
    // // const url = 'http://localhost:8000'; // Replace with your actual Kong API endpoint
    // const data = {
    //   // Your data here as key-value pairs
    // };

    // fetch(url, {
    //   method: 'POST', // Specifying the request method
    //   mode: 'no-cors', // Prevents CORS error
    //   headers: {
    //     'Content-Type': 'application/json', // Specifying content type
    //     // Include additional headers if needed, like authentication tokens
    //   },
    //   body: JSON.stringify(data), // Converting JavaScript object to JSON
    // })
    // .then(response => response.json()) // Parsing the JSON response
    // .then(data => console.log('Success:', data)) // Handling the successful response
    // .catch((error) => console.error('Error:', error)); // Handling errors

      this.getChatHistory();
      this.showChatContent(this.chatId);
    }
    newMessageText: string = '';


  createNewChat() {
    // Example chat object. Adjust the values according to your needs.
    const newChat: any = {
      startDate: new Date(), // Set the start date to current date/time
      endDate: new Date(),   // Adjust this as per your logic
      rating: 0              // Default rating (adjust as needed)
    };

    this.http.post('http://localhost:8080/chat', newChat).subscribe(
      (data: any) => {
        console.log('Chat created:', data);
        this.getChatHistory(); // Reload the chat history to include the new chat
      },
      (error: any) => {
        console.error('Error creating chat:', error);
      }
    );
  }


  sendMessage() {
    if (this.chatId && this.newMessageText) {
      // Example message object. Adjust the values according to your needs.
      const newMessage: any = {
        chat_id: this.chatId,        // Assuming this is the ID of the selected chat
        user_id: 'yourUserId',       // Set the user ID as per your application's logic
        message: this.newMessageText, // The text of the message
        send_date: new Date()        // Current date/time as the send date
      };

      this.http.post(`http://localhost:8080/chat/${this.chatId}/messages`, newMessage).subscribe(
        (data: any) => {
          console.log('Message sent:', data);
          this.showChatContent(this.chatId); // Reload the chat messages to include the new message
        },
        (error: any) => {
          console.error('Error sending message:', error);
        }
      );

      // Clear the message input after sending
      this.newMessageText = '';
    }
  }

  deleteMessage(messageId: string, chatId: string) {
    this.http.delete(`http://localhost:8080/chat/${chatId}/messages/${messageId}`).subscribe(
      () => {
        console.log('Message deleted:', messageId);
        // Refresh the chat content to reflect the deleted message
        this.showChatContent(chatId);
      },
      (error: any) => {
        console.error('Error deleting message:', error);
      }
    );
  }

  getChatHistory() {
    this.http.get('http://localhost:8080/chat').subscribe(
      (data: any) => {
        this.chats = data;
        console.log(data);
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  showChatContent(chatId: string) {
    this.chatId = chatId
    this.http.get(`http://localhost:8080/chat/${chatId}/messages`).subscribe(
      (data: any) => {
        this.chatMessages = data;
        console.log(data);
      },
      (error: any) => {
        console.error(error);
      }
    );
  }
  loggedIds: { messageId: string, chatId: string } | null = null;

  logMessageId(messageId: string, chatId: string) {
    // Find the message in chatMessages with the given messageId
    const message = this.chatMessages.find(m => m.id === messageId);

    // If the message exists, store the messageId and chatId in it and make the request
    if (message) {
      message.loggedIds = { messageId, chatId };

      // Set loading to true
      message.loading = true;

      this.http.get(`http://localhost:8080/evaluation?prompt=10&chat_id=${chatId}&message_id=${messageId}`)
        .pipe(
          finalize(() => {
            // Set loading to false when the request completes
            message.loading = false;
          })
        )
        .subscribe(
          (data: any) => {
            console.log(data);
            // const regExp = /"quality_score_reason": "([^"]*)"/;
            const regExp = /"*"/;
            const matches = data.result.match(regExp);
            if (matches && matches.length > 1) {
              message.evaluation = { quality_score_reason: matches[1] };
            }
          },
          (error: any) => {
            console.error(error);
          }
        );
    }
  }
  sendMessageAndEvaluate(chatId: string = this.chatId, newMessageText: string = this.newMessageText) {
    console.log('chat and message', chatId, newMessageText);
    const theChatID = chatId || this.chatId;
    const theNewMessageText = newMessageText || this.newMessageText;

    if ((this.chatId && this.newMessageText) || (chatId && newMessageText)) {
      const newMessage: any = {
        chat_id: theChatID,
        user_id: 'yourUserId', // Replace with actual user ID
        message: theNewMessageText,
        send_date: new Date()
      };

      // Step 1: Send the original message
      this.http.post(`http://localhost:8080/chat/${theChatID}/messages`, newMessage).pipe(
        switchMap((data: any) => {
          console.log('Message sent:', data);
          // Step 2: Evaluate the message
          if (data && data.id) {
            return this.http.get(`http://localhost:8080/evaluation?prompt=10&chat_id=${theChatID}&message_id=${data.id}`);
          } else {
            return throwError('No message ID returned');
          }
        }),
        finalize(() => {
          this.newMessageText = ''; // Clear the message input
        })
      ).subscribe(
        (evaluationData: any) => {
          console.log('Evaluation data:', evaluationData);
          // Step 3: Send a new message with the evaluation result
          this.sendEvaluationResult(evaluationData, theChatID)  // Use the chat ID from the original message;
        },
        (error: any) => {
          console.error('Error in message sending or evaluation:', error);
        }
      );
    }
  }

  
  sendEvaluationResult(evaluationData: any, chatId: string) {
    // Use a regex to extract the content after "Evaluation result: {"result":"
    const regexPattern = /"result":"([^"]*)"/;


    const evaluationDataString = JSON.stringify(evaluationData);
    const matches = evaluationDataString.match(regexPattern);
    let evaluationResult = '';

    if (matches && matches.length > 1) {
      evaluationResult = matches[1]; // The captured group from the regex

      // Clean new lines and unusual characters
      // evaluationResult = evaluationResult.replace(/[\r\n]+/g, ' ').replace(/[^\w\s.,;!?()-]/g, '');
      evaluationResult = evaluationResult
      // .replace(/\n/, '<br>') // Remove 'n' at the start of the string
      .replace(/(?:^|\s)n/g, ' ') // Replace 'n' at the start or after a space
      .replace(/[^\w\s.,;!?()-]/g, '') // Remove unusual characters
      .trim(); // Trim leading and trailing whitespace
    
    } else {
      evaluationResult = 'No detailed result found';
    }

    const responseMessage: any = {
      chat_id: chatId,
      user_id: 'responseSystem', // Or any appropriate user ID
      message: evaluationResult, // Use the cleaned extracted result
      send_date: new Date()
    };

    this.http.post(`http://localhost:8080/chat/${chatId}/messages`, responseMessage).subscribe(
      (data: any) => {
        console.log('Response message sent:', data);
        // Refresh chat messages to show the new response
        this.showChatContent(chatId);
      },
      (error: any) => {
        console.error('Error sending response message:', error);
      }
    );
  }


}
