// prompt-dialog.component.ts
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface Prompt {
  ID: number;
  Name: string;
  Content: string;
}

@Component({
  selector: 'app-prompt-dialog',
  templateUrl: './prompt-dialog.component.html',
  styleUrls: ['./prompt-dialog.component.scss']
})
export class PromptDialogComponent {
  prompt: Prompt;

  constructor(
    public dialogRef: MatDialogRef<PromptDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.prompt = data.mode === 'edit' ? { ...data.prompt } : { ID: 0, Name: '', Content: '' };
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
