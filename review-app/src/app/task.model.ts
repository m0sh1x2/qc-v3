export interface Task {
    ID: number;
    CreatedAt: string;
    UpdatedAt: string;
    DeletedAt: string | null;
    Description: string;
    Prompt: string;
    Commands: string;
}
