import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';

import { Task } from '../task.model';
import { EditTaskDialogComponent } from '../edit-task-dialog/edit-task-dialog.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  dataSource: MatTableDataSource<Task> = new MatTableDataSource<Task>();
  displayedColumns: string[] = ['id', 'description', 'prompt', 'commands', 'createdAt', 'updatedAt', 'actions'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  tasks: Task[] = [];

  constructor(private http: HttpClient, private dialog: MatDialog) { }

  ngOnInit() {
    this.getTasks();
  }

  getTasks() {
    this.http.get<Task[]>('http://localhost:8085/tasks/').subscribe(
      (response) => {
        this.tasks = response;
        this.dataSource = new MatTableDataSource<Task>(this.tasks);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (error) => {
        console.error('Error retrieving tasks:', error);
      }
    );
  }

  createTask(task: Task) {
    this.http.post<Task>('http://localhost:8085/tasks/', task).subscribe(
      (response) => {
        this.tasks.push(response);
        this.dataSource.data = this.tasks;
      },
      (error) => {
        console.error('Error creating task:', error);
      }
    );
  }
  runTask(task: Task) {
    // Implement logic for running the task
  }
  updateTask(task: Task) {
    this.http.put<Task>(`http://localhost:8085/tasks/${task.ID}`, task).subscribe(
      (response) => {
        const index = this.tasks.findIndex((t) => t.ID === response.ID);
        if (index !== -1) {
          this.tasks[index] = response;
          this.dataSource.data = this.tasks;
        }
      },
      (error) => {
        console.error('Error updating task:', error);
      }
    );
  }

  editTask(task: Task) {
    const dialogRef = this.dialog.open(EditTaskDialogComponent, {
      width: '400px',
      data: { task: { ...task } } // Pass a copy of the task to the dialog
    });

    dialogRef.afterClosed().subscribe((result: Task | undefined) => {
      if (result) {
        this.updateTask(result);
      }
    });
  }
  deleteTask(task: Task) {
    this.http.delete(`http://localhost:8085/tasks/${task.ID}`).subscribe(
      () => {
        this.tasks = this.tasks.filter((t) => t.ID !== task.ID);
        this.dataSource.data = this.tasks;
      },
      (error) => {
        console.error('Error deleting task:', error);
      }
    );
  }

  toggleDetails() {
    // Implement toggle logic if needed
  }

  sortData(sort: Sort) {
    // Sort the data source based on the selected column and direction
    const data = this.tasks.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'description':
          return this.compare(a.Description, b.Description, isAsc);
        case 'prompt':
          return this.compare(a.Prompt, b.Prompt, isAsc);
        case 'commands':
          return this.compare(a.Commands, b.Commands, isAsc);
        default:
          return 0;
      }
    });
  }

  private compare(a: string, b: string, isAsc: boolean) {
    return (a.toLowerCase() < b.toLowerCase() ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
