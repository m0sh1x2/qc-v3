import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../review.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.scss']
})
export class ReviewListComponent implements OnInit {
  displayedColumns: string[] = ['topic', 'rating_tags', 'performance'];
  expandedColumns: string[] = ['topic', 'support_agent_quality_score', 'quality_score_reason'];

  dataSource!: MatTableDataSource<any>;
  showDetails: boolean = false;
  trollCounter = 0;

  getIcon(score: number): string {
    if (score >= 8) {
      return 'thumb_up_alt';
    } else if (score >= 6) {
      return 'thumb_up';
    } else if (score >= 4) {
      return 'check_circle';
    } else if (score >= 2) {
      return 'info';
    } else {
      return 'thumb_down_alt';
    }
  }

  getColor(score: number): string {
    if (score >= 8) {
      return 'green';
    } else if (score >= 6) {
      return 'lightgreen';
    } else if (score >= 4) {
      return 'yellow';
    } else if (score >= 2) {
      return 'orange';
    } else {
      return 'red';
    }
  }

  constructor(
    private reviewService: ReviewService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {

    this.refreshData();
    setInterval(() => this.refreshData(), 20000);
  }

  toggleDetails() {
    this.showDetails = !this.showDetails;
    if (this.showDetails) {
      this.displayedColumns = ['topic', 'short_summary', 'rating_tags', 'support_agent_quality_score', 'performance', 'quality_score_reason'];
    } else {
      this.displayedColumns = ['topic', 'rating_tags', 'performance'];
    }
  }

  private refreshData(count: number = 0) {
    this.reviewService.getReviews().subscribe(data => {

      // Play a sound effect every 5th time the function is called
      this.trollCounter++;
      console.log(this.trollCounter)
      if (this.trollCounter % 5 === 0) {
        this.snackBar.open('Stop it, get some help!', undefined, {
          duration: 5000
        });
        const audio = new Audio('assets/stop.mp3');
        audio.play();
        this.trollCounter = 0
      } else {
        // this.dataSource = new MatTableDataSource(data);
        this.dataSource = new MatTableDataSource(data.reverse()); // Reverse the order of the data array

        this.snackBar.open('Using latest data!', 'Refresh', {
          duration: 5000
        }).onAction().subscribe(() => this.refreshData());

      }
    });
  }

}
