import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

import { Prompt, PromptDialogComponent } from './../prompt-dialog/prompt-dialog.component';

@Component({
  selector: 'app-prompts',
  templateUrl: './prompts.component.html',
  styleUrls: ['./prompts.component.scss']
})
export class PromptsComponent implements OnInit {
  dataSource: MatTableDataSource<Prompt> = new MatTableDataSource<Prompt>([]);
  displayedColumns: string[] = ['id', 'name', 'content', 'actions'];

  hardcodedPromptSource: string = "http://localhost:8080";

  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  constructor(private dialog: MatDialog, private http: HttpClient) { }

  ngOnInit() {
    this.getPrompts();
  }

  getPrompts() {
    this.http.get(this.hardcodedPromptSource + '/prompts').subscribe(
      (data: any) => {
        // Handle the response data here
        this.dataSource.data = data;
        this.dataSource.sort = this.sort; // Add this line to apply sorting

        console.log(data);
      },
      (error: any) => {
        // Handle errors here
        console.error(error);
      }
    );
  }

  openDialog() {
    const dialogRef = this.dialog.open(PromptDialogComponent, {
      width: '900px',
      data: { mode: 'add' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.createPrompt(result);
      }
    });
  }

  editPrompt(prompt: Prompt) {
    const dialogRef = this.dialog.open(PromptDialogComponent, {
      width: '75vw',
      height: '80vh',
      data: { mode: 'edit', prompt }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.updatePrompt(prompt.ID, result);
      }
    });
  }

  deletePrompt(id: number) {
    if (confirm('Are you sure you want to delete this prompt?')) {
      this.http.delete(this.hardcodedPromptSource + `/prompt/${id}`).subscribe(() => {
        this.getPrompts(); // Re-fetch prompts
      });
    }
  }

  createPrompt(prompt: Prompt) {
    this.http.post(this.hardcodedPromptSource + '/prompt', prompt).subscribe(() => {
      this.getPrompts(); // Re-fetch prompts
    });
  }

  updatePrompt(id: number, prompt: Prompt) {
    this.http.put(this.hardcodedPromptSource + `/prompt/${id}`, prompt).subscribe(() => {
      this.getPrompts(); // Re-fetch prompts
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }
}
