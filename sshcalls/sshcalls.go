package sshcalls

import (
	"database/sql"
	"encoding/json"
	"net"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/ssh"
)

type SSHClient struct {
	Client *ssh.Client
	Db     *sql.DB
}

type CommandHistory struct {
	ID                 int64     `json:"id"`
	HostIP             string    `json:"host_ip"`
	ExecutedCommand    string    `json:"executed_command"`
	ExecutedResult     string    `json:"executed_result"`
	RequesterIP        string    `json:"requester_ip"`
	ExecutionTimestamp time.Time `json:"execution_timestamp"`
}

func NewSSHClient(user, password, host string, port int, db *sql.DB) (*SSHClient, error) {
	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.Password(password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	client, err := ssh.Dial("tcp", net.JoinHostPort(host, strconv.Itoa(port)), config)
	if err != nil {
		return nil, err
	}

	return &SSHClient{Client: client, Db: db}, nil
}

func (s *SSHClient) RunCommand(cmd string) (string, error) {
	session, err := s.Client.NewSession()
	if err != nil {
		return "", err
	}
	defer session.Close()

	out, err := session.Output(cmd)
	if err != nil {
		return "", err
	}

	return string(out), nil
}

func (s *SSHClient) SaveCommandHistory(ch CommandHistory) error {
	query := "INSERT INTO command_history (host_ip, executed_command, executed_result, requester_ip, execution_timestamp) VALUES (?, ?, ?, ?, ?)"

	_, err := s.Db.Exec(query, ch.HostIP, ch.ExecutedCommand, ch.ExecutedResult, ch.RequesterIP, ch.ExecutionTimestamp)

	return err
}

func (s *SSHClient) GetCommandHistory(id int64) (CommandHistory, error) {
	var ch CommandHistory
	query := "SELECT * FROM command_history WHERE id = ?"
	row := s.Db.QueryRow(query, id)

	err := row.Scan(&ch.ID, &ch.HostIP, &ch.ExecutedCommand, &ch.ExecutedResult, &ch.RequesterIP, &ch.ExecutionTimestamp)
	return ch, err
}

func (s *SSHClient) GetAllCommandHistories() ([]CommandHistory, error) {
	var histories []CommandHistory
	query := "SELECT * FROM command_history"

	rows, err := s.Db.Query(query)
	if err != nil {
		return histories, err
	}

	for rows.Next() {
		var ch CommandHistory
		err = rows.Scan(&ch.ID, &ch.HostIP, &ch.ExecutedCommand, &ch.ExecutedResult, &ch.RequesterIP, &ch.ExecutionTimestamp)
		if err != nil {
			return histories, err
		}
		histories = append(histories, ch)
	}

	return histories, nil
}

func (s *SSHClient) UpdateCommandHistory(ch CommandHistory) error {
	query := "UPDATE command_history SET host_ip = ?, executed_command = ?, executed_result = ?, requester_ip = ?, execution_timestamp = ? WHERE id = ?"
	_, err := s.Db.Exec(query, ch.HostIP, ch.ExecutedCommand, ch.ExecutedResult, ch.RequesterIP, ch.ExecutionTimestamp, ch.ID)
	return err
}

func (s *SSHClient) DeleteCommandHistory(id int64) error {
	query := "DELETE FROM command_history WHERE id = ?"
	_, err := s.Db.Exec(query, id)
	return err
}

func (s *SSHClient) HandleRunCommand(w http.ResponseWriter, r *http.Request) {
	var ch CommandHistory
	err := json.NewDecoder(r.Body).Decode(&ch)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Run the command
	result, err := s.RunCommand(ch.ExecutedCommand)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Add the result to the command history
	ch.ExecutedResult = result

	// Save the command history
	err = s.SaveCommandHistory(ch)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Write the result to the response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ch)
}

func (s *SSHClient) HandleGetCommandHistory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ch, err := s.GetCommandHistory(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ch)
}

func (s *SSHClient) HandleGetAllCommandHistories(w http.ResponseWriter, r *http.Request) {
	ch, err := s.GetAllCommandHistories()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ch)
}

func (s *SSHClient) HandleUpdateCommandHistory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var ch CommandHistory
	err = json.NewDecoder(r.Body).Decode(&ch)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ch.ID = id
	err = s.UpdateCommandHistory(ch)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ch)
}

func (s *SSHClient) HandleDeleteCommandHistory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = s.DeleteCommandHistory(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
