package evaluation

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"
)

type API struct {
	db *sql.DB
}

func NewAPI(db *sql.DB) *API {
	return &API{db: db}
}

type Evaluation struct {
	PromptContent string `json:"prompt_content"`
	ChatMessage   string `json:"chat_message"`
}

func (api *API) GetPrompt(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	promptContent, err := api.fetchPromptContentByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(promptContent)
}

func (api *API) fetchPromptContentByID(id string) (string, error) {
	var promptContent string
	err := api.db.QueryRow("SELECT content FROM prompts WHERE id = $1", id).Scan(&promptContent)
	if err != nil {
		return "", err
	}
	return promptContent, nil
}

func (api *API) GetChatMessage(w http.ResponseWriter, r *http.Request) {
	chatID := mux.Vars(r)["chat_id"]
	messageID := mux.Vars(r)["message_id"]

	chatMessage, err := api.fetchChatMessage(chatID, messageID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(chatMessage)
}

func (api *API) fetchChatMessage(chatID, messageID string) (string, error) {
	var chatMessage string
	err := api.db.QueryRow("SELECT message FROM messages WHERE chat_id = $1 AND id = $2", chatID, messageID).Scan(&chatMessage)
	if err != nil {
		return "", err
	}
	return chatMessage, nil
}

func (api *API) HandlePrompt(w http.ResponseWriter, r *http.Request) {
	promptID := r.URL.Query().Get("prompt")
	evaluation := Evaluation{}

	promptContent, err := api.fetchPromptContentByID(promptID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	evaluation.PromptContent = promptContent

	chatID := r.URL.Query().Get("chat_id")
	messageID := r.URL.Query().Get("message_id")

	chatMessage, err := api.fetchChatMessage(chatID, messageID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	evaluation.ChatMessage = chatMessage

	log.Printf("Evaluation: %+v\n", evaluation)

	w.Header().Set("Content-Type", "application/json")
	// json.NewEncoder(w).Encode(evaluation)

	// URL encode the prompt content value
	// The FNX_MESSAGE_EVAL_REPLACEMENT is a placeholder which must be present in the prompts that will require chat evaluation
	mutatedChatEvaluationPrompt := strings.Replace(evaluation.PromptContent, "FNX_MESSAGE_EVAL_REPLACEMENT", evaluation.ChatMessage, 1)

	// encodedPromptContent := url.QueryEscape(evaluation.PromptContent)
	encodedPromptContent := url.QueryEscape(mutatedChatEvaluationPrompt)

	fmt.Println(mutatedChatEvaluationPrompt)
	// Send GET request to "/prompt?prompt=RESPONSE_FROM_EVALUATION_HERE"
	evalPromptURL := fmt.Sprintf("http://localhost:8080/prompt?prompt=%s", encodedPromptContent)
	resp, err := http.Get(evalPromptURL)
	if err != nil {
		log.Printf("Failed to send GET request to %s: %v", evalPromptURL, err)
		return
	}
	defer resp.Body.Close()

	// Read the response body
	responseBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Failed to read response body: %v", err)
		return
	}

	// Write the response as the HTTP response
	w.Header().Set("Content-Type", "text/plain")
	w.Write(responseBytes)
}
