package models

type Review struct {
	ID                  int      `json:"id"`
	Topic               string   `json:"topic"`
	RatingTags          []string `json:"rating_tags"`
	ShortSummary        string   `json:"short_summary"`
	SupportAgentQuality int      `json:"support_agent_quality_score"`
	QualityScoreReason  string   `json:"quality_score_reason"`
}
