package chat

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type Chat struct {
	ID           string    `json:"id"`
	StartDate    time.Time `json:"startDate"`
	EndDate      time.Time `json:"endDate"`
	Rating       int       `json:"rating"`
	ChatMessages []Message `json:"chat_messages"`
}

type Message struct {
	ID       string    `json:"id"`
	ChatID   string    `json:"chat_id"`
	UserID   string    `json:"user_id"`
	Message  string    `json:"message"`
	SendDate time.Time `json:"send_date"`
}

type API struct {
	db *sql.DB
}

func NewAPI(db *sql.DB) *API {
	return &API{db: db}
}

func (api *API) GetChats(w http.ResponseWriter, r *http.Request) {
	rows, err := api.db.Query("SELECT id, start_date, end_date, rating FROM chats")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var chats []Chat
	for rows.Next() {
		var chat Chat
		if err := rows.Scan(&chat.ID, &chat.StartDate, &chat.EndDate, &chat.Rating); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		chats = append(chats, chat)
	}

	if err := rows.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(chats)
}

func (api *API) GetChat(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	var chat Chat
	err := api.db.QueryRow("SELECT id, start_date, end_date, rating FROM chats WHERE id = $1", id).Scan(&chat.ID, &chat.StartDate, &chat.EndDate, &chat.Rating)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	chat.ChatMessages, err = api.getChatMessages(chat.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(chat)
}

func (api *API) CreateChat(w http.ResponseWriter, r *http.Request) {
	var chat Chat
	json.NewDecoder(r.Body).Decode(&chat)

	_, err := api.db.Exec("INSERT INTO chats (start_date, end_date, rating) VALUES ($1, $2, $3)", chat.StartDate, chat.EndDate, chat.Rating)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(chat)
}

func (api *API) UpdateChat(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	var chat Chat
	json.NewDecoder(r.Body).Decode(&chat)
	chat.ID = id

	_, err := api.db.Exec("UPDATE chats SET start_date = $1, end_date = $2, rating = $3 WHERE id = $4", chat.StartDate, chat.EndDate, chat.Rating, chat.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(chat)
}

func (api *API) DeleteChat(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	_, err := api.db.Exec("DELETE FROM chats WHERE id = $1", id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(map[string]string{"message": "chat deleted"})
}

func (api *API) getChatMessages(chatID string) ([]Message, error) {
	rows, err := api.db.Query("SELECT id, chat_id, user_id, message, send_date FROM messages WHERE chat_id = $1", chatID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var messages []Message
	for rows.Next() {
		var message Message
		if err := rows.Scan(&message.ID, &message.ChatID, &message.UserID, &message.Message, &message.SendDate); err != nil {
			return nil, err
		}
		messages = append(messages, message)
	}

	return messages, nil
}

func (api *API) GetChatMessages(w http.ResponseWriter, r *http.Request) {
	chatID := mux.Vars(r)["chat_id"]

	messages, err := api.getChatMessages(chatID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(messages)
}
func (api *API) GetChatMessage(w http.ResponseWriter, r *http.Request) {
	chatID := mux.Vars(r)["chat_id"]
	messageID := mux.Vars(r)["message_id"]

	var message Message
	err := api.db.QueryRow("SELECT id, chat_id, user_id, message, send_date FROM messages WHERE chat_id = $1 AND id = $2", chatID, messageID).Scan(&message.ID, &message.ChatID, &message.UserID, &message.Message, &message.SendDate)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(message)
}

func (api *API) CreateMessage(w http.ResponseWriter, r *http.Request) {
    chatID := mux.Vars(r)["chat_id"]

    var message Message
    err := json.NewDecoder(r.Body).Decode(&message)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }
    message.ChatID = chatID

    // Use QueryRow with the RETURNING clause to get the ID
    err = api.db.QueryRow("INSERT INTO messages (chat_id, user_id, message, send_date) VALUES ($1, $2, $3, $4) RETURNING id", message.ChatID, message.UserID, message.Message, message.SendDate).Scan(&message.ID)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    json.NewEncoder(w).Encode(message)
}

func (api *API) UpdateMessage(w http.ResponseWriter, r *http.Request) {
	chatID := mux.Vars(r)["chat_id"]
	messageID := mux.Vars(r)["message_id"]

	var message Message
	json.NewDecoder(r.Body).Decode(&message)
	message.ChatID = chatID
	message.ID = messageID

	_, err := api.db.Exec("UPDATE messages SET user_id = $1, message = $2, send_date = $3 WHERE chat_id = $4 AND id = $5", message.UserID, message.Message, message.SendDate, message.ChatID, message.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(message)
}

func (api *API) DeleteMessage(w http.ResponseWriter, r *http.Request) {
	chatID := mux.Vars(r)["chat_id"]
	messageID := mux.Vars(r)["message_id"]

	_, err := api.db.Exec("DELETE FROM messages WHERE chat_id = $1 AND id = $2", chatID, messageID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(map[string]string{"message": "message deleted"})
}
