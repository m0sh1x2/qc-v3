package score

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Score represents a score entity
type Score struct {
	ID          int    `json:"id"`
	Value       int    `json:"value"`
	User        string `json:"user"`
	Agent       string `json:"agent"`
	Message     string `json:"message"`
	ScoreReason string `json:"score_reason"`
	PromptID    int    `json:"prompt_id"`
}

// API represents the score management API
type API struct {
	db *sql.DB
}

// NewAPI creates a new instance of the score management API
func NewAPI(db *sql.DB) *API {
	return &API{db: db}
}

// GetScores retrieves all scores
func (api *API) GetScores(w http.ResponseWriter, r *http.Request) {
	rows, err := api.db.Query("SELECT id, value, \"user\", agent, message, score_reason, prompt_id FROM scores")
	if err != nil {
		log.Printf("failed to get scores: %v", err)
		http.Error(w, "failed to get scores", http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	scores := make([]Score, 0)
	for rows.Next() {
		score := Score{}
		err := rows.Scan(&score.ID, &score.Value, &score.User, &score.Agent, &score.Message, &score.ScoreReason, &score.PromptID)
		if err != nil {
			log.Printf("failed to scan score: %v", err)
			http.Error(w, "failed to get scores", http.StatusInternalServerError)
			return
		}
		scores = append(scores, score)
	}

	response, err := json.Marshal(scores)
	if err != nil {
		log.Printf("failed to marshal scores: %v", err)
		http.Error(w, "failed to get scores", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// GetScore retrieves a specific score by ID
func (api *API) GetScore(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	scoreID, err := strconv.Atoi(params["id"])
	if err != nil {
		log.Printf("invalid score ID: %v", err)
		http.Error(w, "invalid score ID", http.StatusBadRequest)
		return
	}

	score := Score{}
	err = api.db.QueryRow("SELECT id, value,user, agent, message, score_reason, prompt_id FROM scores WHERE id = $1", scoreID).Scan(&score.ID, &score.Value, &score.User, &score.Agent, &score.Message, &score.ScoreReason, &score.PromptID)
	if err != nil {
		log.Printf("failed to get score: %v", err)
		http.Error(w, "failed to get score", http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(score)
	if err != nil {
		log.Printf("failed to marshal score: %v", err)
		http.Error(w, "failed to get score", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// CreateScore creates a new score
func (api *API) CreateScore(w http.ResponseWriter, r *http.Request) {
	var score Score
	err := json.NewDecoder(r.Body).Decode(&score)
	if err != nil {
		log.Printf("failed to decode score: %v", err)
		http.Error(w, "failed to create score", http.StatusBadRequest)
		return
	}

	result, err := api.db.Exec("INSERT INTO scores (value, \"user\", agent, message, score_reason, prompt_id) VALUES ($1, $2, $3, $4, $5, $6)", score.Value, score.User, score.Agent, score.Message, score.ScoreReason, score.PromptID)
	if err != nil {
		log.Printf("failed to create score: %v", err)
		http.Error(w, "failed to create score", http.StatusInternalServerError)
		return
	}

	scoreID, _ := result.LastInsertId()
	score.ID = int(scoreID)

	response, err := json.Marshal(score)
	if err != nil {
		log.Printf("failed to marshal score: %v", err)
		http.Error(w, "failed to create score", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

// UpdateScore updates an existing score
func (api *API) UpdateScore(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	scoreID, err := strconv.Atoi(params["id"])
	if err != nil {
		log.Printf("invalid score ID: %v", err)
		http.Error(w, "invalid score ID", http.StatusBadRequest)
		return
	}

	var score Score
	err = json.NewDecoder(r.Body).Decode(&score)
	if err != nil {
		log.Printf("failed to decode score: %v", err)
		http.Error(w, "failed to update score", http.StatusBadRequest)
		return
	}

	_, err = api.db.Exec("UPDATE scores SET value = $1, user = $2, agent = $3, message = $4, score_reason = $5, prompt_id = $6 WHERE id = $7", score.Value, score.User, score.Agent, score.Message, score.ScoreReason, score.PromptID, scoreID)
	if err != nil {
		log.Printf("failed to update score: %v", err)
		http.Error(w, "failed to update score", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// DeleteScore deletes a score by ID
func (api *API) DeleteScore(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	scoreID, err := strconv.Atoi(params["id"])
	if err != nil {
		log.Printf("invalid score ID: %v", err)
		http.Error(w, "invalid score ID", http.StatusBadRequest)
		return
	}

	_, err = api.db.Exec("DELETE FROM scores WHERE id = $1", scoreID)
	if err != nil {
		log.Printf("failed to delete score: %v", err)
		http.Error(w, "failed to delete score", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
