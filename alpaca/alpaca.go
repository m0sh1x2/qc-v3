package alpaca

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	host = "192.168.1.135:5000"
	uri  = "http://" + host + "/api/v1/generate"
)

func Generate(prompt string) (string, error) {
	fmt.Println("Request made...")
	request := map[string]interface{}{
		"prompt":               prompt,
		"max_new_tokens":       350,
		"do_sample":            true,
		"temperature":          0.1,
		"top_p":                0.1,
		"typical_p":            1,
		"repetition_penalty":   1.18,
		"top_k":                40,
		"min_length":           0,
		"no_repeat_ngram_size": 0,
		"num_beams":            1,
		"penalty_alpha":        0,
		"length_penalty":       1,
		"early_stopping":       false,
		"seed":                 -1,
		"add_bos_token":        true,
		"truncation_length":    2048,
		"ban_eos_token":        false,
		"skip_special_tokens":  true,
		"stopping_strings":     []string{},
	}

	requestBytes, err := json.Marshal(request)
	if err != nil {
		return "", err
	}

	response, err := http.Post(uri, "application/json", bytes.NewBuffer(requestBytes))
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	if response.StatusCode == 200 {
		responseData, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return "", err
		}

		var result map[string]interface{}
		if err := json.Unmarshal(responseData, &result); err != nil {
			return "", err
		}

		resultText := result["results"].([]interface{})[0].(map[string]interface{})["text"].(string)
		return resultText, nil
	} else {
		return "", fmt.Errorf("Unexpected status code: %d", response.StatusCode)
	}
}
