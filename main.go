package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
	"gitlab.com/m0sh1x2/qc-v3/chat"
	"gitlab.com/m0sh1x2/qc-v3/evaluation"
	"gitlab.com/m0sh1x2/qc-v3/prompts"
	"gitlab.com/m0sh1x2/qc-v3/score"
	// "gitlab.com/m0sh1x2/qc-v3/sshcalls"
)

type Review struct {
	ID                  int      `json:"id"`
	Topic               string   `json:"topic"`
	RatingTags          []string `json:"rating_tags"`
	ShortSummary        string   `json:"short_summary"`
	SupportAgentQuality int      `json:"support_agent_quality_score"`
	QualityScoreReason  string   `json:"quality_score_reason"`
}

var db *sql.DB

func runPrompt(prompt, host, uri string) string {
	stoppingStrings := []string{"CLIENT:", "END:"} // Example stopping strings

	request := map[string]interface{}{
		"prompt":               prompt,
		"max_new_tokens":       150,
		"do_sample":            true,
		"temperature":          0.1,
		"top_p":                0.1,
		"typical_p":            1,
		"repetition_penalty":   1.18,
		"top_k":                40,
		"min_length":           0,
		"no_repeat_ngram_size": 0,
		"num_beams":            1,
		"penalty_alpha":        0,
		"length_penalty":       1,
		"early_stopping":       false,
		"seed":                 -1,
		"add_bos_token":        true,
		"truncation_length":    2048,
		"ban_eos_token":        false,
		"skip_special_tokens":  true,
		"stopping_strings":     stoppingStrings,
	}

	requestBody, _ := json.Marshal(request)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", uri, strings.NewReader(string(requestBody)))
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		var response map[string]interface{}
		err = json.Unmarshal(bodyBytes, &response)
		if err != nil {
			panic(err)
		}

		result := response["results"].([]interface{})[0].(map[string]interface{})["text"].(string)
		fmt.Println(result)
		return result
	}
	return "fail"
}
func main() {
	fmt.Println("started?")

	var err error
	db, err = sql.Open("postgres", dbConnectionString())
	if err != nil {
		log.Fatalf("failed to open database connection: %v", err)
	}
	defer db.Close()

	if err = createTable(db); err != nil {
		log.Fatalf("failed to create table: %v", err)
	}

	router := mux.NewRouter()

	// Old routes
	router.HandleFunc("/reviews", handleRequest).Methods("GET")
	router.HandleFunc("/reviews", handleRequest).Methods("POST")

	router.HandleFunc("/prompt", handlePrompt).Methods("GET")
	router.HandleFunc("/prompts", getPrompts).Methods("GET")

	router.HandleFunc("/prompt", createPrompt).Methods("POST")
	router.HandleFunc("/prompt/{id}", getPrompt).Methods("GET")
	router.HandleFunc("/prompt/{id}", updatePrompt).Methods("PUT")
	router.HandleFunc("/prompt/{id}", deletePrompt).Methods("DELETE")

	// New chat routes
	chatAPI := chat.NewAPI(db)
	router.HandleFunc("/chat", chatAPI.GetChats).Methods("GET")
	router.HandleFunc("/chat/{id}", chatAPI.GetChat).Methods("GET")
	router.HandleFunc("/chat", chatAPI.CreateChat).Methods("POST")
	router.HandleFunc("/chat/{id}", chatAPI.UpdateChat).Methods("PUT")
	router.HandleFunc("/chat/{id}", chatAPI.DeleteChat).Methods("DELETE")


	// You can create a chat with this curl command:
	// curl -X POST -H "Content-Type: application/json" -d '{"start_date":"2021-01-01T00:00:00Z","end_date":"2021-01-01T00:00:00Z","rating":5}' http://localhost:8080/chat
	// And insert some example messages with this curl command:
	// curl -X POST -H "Content-Type: application/json" -d '{"chat_id":1,"user_id":"user1","message":"hello","send_date":"2021-01-01T00:00:00Z"}' http://localhost:8080/chat/1/messages
	// This is a for loop in bash that will make a curl request and generate 10 messages from both sides:
	// for i in {1..10}; do curl -X POST -H "Content-Type: application/json" -d '{"chat_id":1,"user_id":"user1","message":"hello","send_date":"2021-01-01T00:00:00Z"}' http://localhost:8080/chat/1/messages; curl -X POST -H "Content-Type: application/json" -d '{"chat_id":1,"user_id":"user2","message":"hello","send_date":"2021-01-01T00:00:00Z"}' http://localhost:8080/chat/1/messages; done

	// You might get an error from the loop above like this pq: insert or update on table "messages" violates foreign key constraint "messages_chat_id_fkey" and the solution would be:
	// curl -X POST -H "Content-Type: application/json" -d '{"start_date":"2021-01-01T00:00:00Z","end_date":"2021-01-01T00:00:00Z","rating":5}' http://localhost:8080/chat
	// curl -X POST -H "Content-Type: application/json" -d '{"chat_id":1,"user_id":"user1","message":"hello","send_date":"2021-01-01T00:00:00Z"}' http://localhost:8080/chat/1/messages
	// curl -X POST -H "Content-Type: application/json" -d '{"chat_id":1,"user_id":"user2","message":"hello","send_date":"2021-01-01T00:00:00Z"}' http://localhost:8080/chat/1/messages
		
	// To remove the chat and messages you can use these curl commands:
	// curl -X DELETE http://localhost:8080/chat/1


	// New messages routes
	router.HandleFunc("/chat/{chat_id}/messages", chatAPI.GetChatMessages).Methods("GET")
	router.HandleFunc("/chat/{chat_id}/messages/{message_id}", chatAPI.GetChatMessage).Methods("GET")
	router.HandleFunc("/chat/{chat_id}/messages", chatAPI.CreateMessage).Methods("POST")
	router.HandleFunc("/chat/{chat_id}/messages/{message_id}", chatAPI.UpdateMessage).Methods("PUT")
	router.HandleFunc("/chat/{chat_id}/messages/{message_id}", chatAPI.DeleteMessage).Methods("DELETE")

	// New score routes
	scoreAPI := score.NewAPI(db)
	router.HandleFunc("/score", scoreAPI.GetScores).Methods("GET")
	router.HandleFunc("/score/{id}", scoreAPI.GetScore).Methods("GET")
	router.HandleFunc("/score", scoreAPI.CreateScore).Methods("POST")
	router.HandleFunc("/score/{id}", scoreAPI.UpdateScore).Methods("PUT")
	router.HandleFunc("/score/{id}", scoreAPI.DeleteScore).Methods("DELETE")

	// sshAPI, err := sshcalls.NewSSHClient("vagrant", "vagrant", "192.168.121.226", 22, db)

	// if err != nil {
	// 	log.Fatalf("failed to create SSH client: %v", err)
	// }

	// router.HandleFunc("/command", sshAPI.HandleRunCommand).Methods("POST")
	// router.HandleFunc("/command/{id}", sshAPI.HandleGetCommandHistory).Methods("GET")
	// router.HandleFunc("/command", sshAPI.HandleGetAllCommandHistories).Methods("GET")
	// router.HandleFunc("/command/{id}", sshAPI.HandleUpdateCommandHistory).Methods("PUT")
	// router.HandleFunc("/command/{id}", sshAPI.HandleDeleteCommandHistory).Methods("DELETE")

	// Evaluation route
	evaluationAPI := evaluation.NewAPI(db)
	router.HandleFunc("/evaluation", evaluationAPI.HandlePrompt).Methods("GET")

	// // Prompt evaluation route
	// router.HandleFunc("/evaluation", handlePromptEvaluation).Methods("GET")

	// corsHandler := cors.Default().Handler(router)
	corsHandler := handlers.CORS(
		handlers.AllowedOrigins([]string{"http://localhost:4200"}),
		handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"}),
		handlers.AllowedHeaders([]string{"Content-Type"}),
	)(router)
	log.Fatal(http.ListenAndServe(":8080", corsHandler))
}

func handlePromptEvaluation(w http.ResponseWriter, r *http.Request) {
	promptIDStr := r.URL.Query().Get("prompt_id")
	// messageToEvaluate := r.URL.Query().Get("message_to_evaluate")

	promptID, err := strconv.Atoi(promptIDStr)
	if err != nil {
		log.Printf("invalid prompt ID: %v", err)
		http.Error(w, "invalid prompt ID", http.StatusBadRequest)
		return
	}

	prompt, err := prompts.Read(db, promptID)
	if err != nil {
		log.Printf("failed to read prompt: %v", err)
		http.Error(w, "failed to read prompt", http.StatusInternalServerError)
		return
	}
	if prompt == nil {
		log.Printf("prompt not found")
		http.Error(w, "prompt not found", http.StatusNotFound)
		return
	}

	// Use the prompt and messageToEvaluate as needed

	w.WriteHeader(http.StatusOK)
	// Write your response here
}

func createPrompt(w http.ResponseWriter, r *http.Request) {
	var prompt prompts.Prompt
	err := json.NewDecoder(r.Body).Decode(&prompt)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = prompts.Create(db, &prompt)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func getPrompt(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	prompt, err := prompts.Read(db, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if prompt == nil {
		http.NotFound(w, r)
		return
	}

	json.NewEncoder(w).Encode(prompt)
}

func getPrompts(w http.ResponseWriter, r *http.Request) {
	promptList, err := prompts.ReadAll(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(promptList)
}

func updatePrompt(w http.ResponseWriter, r *http.Request) {
	// Set CORS headers
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "PUT")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	// Check if the request method is OPTIONS (preflight request)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		return
	}

	// Parse the ID from the request parameters
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Decode the JSON payload into a Prompt struct
	var prompt prompts.Prompt
	err = json.NewDecoder(r.Body).Decode(&prompt)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	prompt.ID = id

	// Update the prompt in the database
	err = prompts.Update(db, &prompt)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func deletePrompt(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = prompts.Delete(db, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func handlePrompt(w http.ResponseWriter, r *http.Request) {
	const host = "localhost:5000"
	const uri = "http://" + host + "/api/v1/generate"

	prompt := r.URL.Query().Get("prompt")
	if prompt == "" {
		http.Error(w, "Prompt is required", http.StatusBadRequest)
		return
	}

	result := runPrompt(strings.TrimSpace(prompt), host, uri)

	response := struct {
		Result string `json:"result"`
	}{
		Result: result,
	}

	// Marshal the response object into JSON
	responseJSON, err := json.Marshal(response)
	if err != nil {
		http.Error(w, "Error encoding response", http.StatusInternalServerError)
		return
	}

	// Set the appropriate Content-Type header
	w.Header().Set("Content-Type", "application/json")

	// Write the JSON response to the response writer
	w.WriteHeader(http.StatusOK)
	w.Write(responseJSON)
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		idParam := r.URL.Query().Get("id")
		if idParam != "" {
			id, err := strconv.Atoi(idParam)
			if err != nil {
				http.Error(w, "Invalid id parameter", http.StatusBadRequest)
				return
			}

			review, err := getReviewByID(id)
			if err != nil {
				if err == sql.ErrNoRows {
					http.Error(w, "Review not found", http.StatusNotFound)
				} else {
					http.Error(w, "Error getting review", http.StatusInternalServerError)
				}
				return
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(review)
		} else {
			reviews, err := getReviews()
			if err != nil {
				http.Error(w, "Error getting reviews", http.StatusInternalServerError)
				return
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(reviews)
		}

	case "POST":
		var review Review
		err := json.NewDecoder(r.Body).Decode(&review)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = saveReview(review)
		if err != nil {
			http.Error(w, "Error saving review", http.StatusInternalServerError)
			return
		}

		fmt.Printf("Received review for %s\n", review.Topic)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Review received"))
	case "DELETE":
		idParam := r.URL.Query().Get("id")
		if idParam == "" {
			http.Error(w, "Missing id parameter", http.StatusBadRequest)
			return
		}

		id, err := strconv.Atoi(idParam)
		if err != nil {
			http.Error(w, "Invalid id parameter", http.StatusBadRequest)
			return
		}

		err = deleteReview(id)
		if err != nil {
			http.Error(w, "Error deleting review", http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Review deleted"))
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
}
func getReviewByID(id int) (Review, error) {
	var review Review
	var ratingTags []string

	err := db.QueryRow(`
		SELECT topic, rating_tags, short_summary, support_agent_quality, quality_score_reason
		FROM reviews
		WHERE id = $1
	`, id).Scan(&review.Topic, pq.Array(&ratingTags), &review.ShortSummary, &review.SupportAgentQuality, &review.QualityScoreReason)
	if err != nil {
		return review, err
	}

	review.RatingTags = ratingTags

	return review, nil
}
func getReviews() ([]Review, error) {
	rows, err := db.Query(`
        SELECT id, topic, rating_tags, short_summary, support_agent_quality, quality_score_reason
        FROM reviews
    `)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var reviews []Review
	for rows.Next() {
		var review Review
		var ratingTags []string

		err := rows.Scan(&review.ID, &review.Topic, pq.Array(&ratingTags), &review.ShortSummary, &review.SupportAgentQuality, &review.QualityScoreReason)
		if err != nil {
			return nil, err
		}

		review.RatingTags = ratingTags
		reviews = append(reviews, review)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return reviews, nil
}

func saveReview(review Review) error {
	_, err := db.Exec(`
        INSERT INTO reviews (topic, rating_tags, short_summary, support_agent_quality, quality_score_reason)
            VALUES ($1, $2, $3, $4, $5)`,
		review.Topic, pqArray(review.RatingTags), review.ShortSummary, review.SupportAgentQuality, review.QualityScoreReason)
	return err
}

func deleteReview(id int) error {
	_, err := db.Exec(`
        DELETE FROM reviews WHERE id = $1`,
		id)
	return err
}

func dbConnectionString() string {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")

	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
}

func pqArray(arr []string) string {
	var result string
	for i, val := range arr {
		if i > 0 {
			result += ","
		}
		result += fmt.Sprintf(`"%v"`, val)
	}
	return "{" + result + "}"
}

func createTable(db *sql.DB) error {
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS prompts (
			id SERIAL PRIMARY KEY,
			name VARCHAR(255) NOT NULL,
			content TEXT NOT NULL
		)
	`)
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS reviews (
			id SERIAL PRIMARY KEY,
			topic VARCHAR(255) NOT NULL,
			rating_tags TEXT[],
			short_summary TEXT NOT NULL,
			support_agent_quality INTEGER NOT NULL,
			quality_score_reason TEXT NOT NULL
		)
	`)
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS chats (
			id SERIAL PRIMARY KEY,
			start_date TIMESTAMP NOT NULL,
			end_date TIMESTAMP NOT NULL,
			rating INT NOT NULL
		)
	`)
	if err != nil {
		return err
	}
	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS messages (
			id SERIAL PRIMARY KEY,
			chat_id INT NOT NULL,
			user_id VARCHAR(255) NOT NULL,
			message TEXT NOT NULL,
			send_date TIMESTAMP NOT NULL,
			FOREIGN KEY (chat_id) REFERENCES chats (id) ON DELETE CASCADE
		)
	`)
	if err != nil {
		return err
	}

	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS command_history (
		id SERIAL PRIMARY KEY,
		host_ip VARCHAR(255) NOT NULL,
		executed_command TEXT NOT NULL,
		executed_result TEXT NOT NULL,
		requester_ip VARCHAR(255) NOT NULL,
		execution_timestamp TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
		)
	`)
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS scores (
			id SERIAL PRIMARY KEY,
			value INT,
			"user" TEXT,
			agent TEXT,
			message TEXT,
			score_reason TEXT,
			prompt_id INT
		)
	`)
	if err != nil {
		return err
	}

	return nil
}
